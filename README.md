# cpp-examples

C++ examples of various things.

## Set Up

After cloning or downloading, most programs should be a single file that can be 
compiled with g++, unless otherwise stated.

```
g++ -o 'filename' 'filename.cpp' # compile file
./'filename' # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

