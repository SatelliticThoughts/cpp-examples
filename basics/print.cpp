#include <iostream>

int main(int argc, char **argv)
{
	// Hello World example
	std::cout << "Hello World!" << std::endl;

	// Printing variable example
	std::string text = "This is a test";
	std::cout << text << std::endl;

	// Printing multiple variables
	std::string name = "luke";
	int age = 200;
	std::cout << "My name is " << name << " and i am " << age << 
		" years old" << std::endl;

	return 0;
}

