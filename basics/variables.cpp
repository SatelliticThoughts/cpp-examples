#include <iostream>

int main(int argc, char** argv)
{
	// Integers
	int x = 90;
	int y = 30;

	std::cout << "Integer Calculations, x = " << x << ", y = " << y << 
		"\n\n";

	std::cout << "x + y = " << (x + y) << std::endl; // Addition
	std::cout << "x - y = " << (x - y) << std::endl; // Subtraction
	std::cout << "x * y = " << (x * y) << std::endl; // Multiplication
	std::cout << "x / y = " << (x / y) << std::endl; // Division
	std::cout << "x % y = " << (x % y) << std::endl; // Modulus

	// Floating point numbers
	float a = 6.5312f;
	float b = 2.1f;

	std::cout << "\nFloating point Calculations, a = " << a << ", b = " << 
		b << "\n\n";

	std::cout << "a + b = " << (a + b) << std::endl; // Addition
	std::cout << "a - b = " << (a - b) << std::endl; // Subtraction
	std::cout << "a * b = " << (a * b) << std::endl; // Multiplication
	std::cout << "a / b = " << (a / b) << std::endl; // Division

	// Strings
	std::cout << "\nString Manipulation\n";

	std::string hello = "hello";
	std::cout << hello << std::endl;

	hello = hello + " world";
	std::cout << hello << std::endl;

	std::cout << hello.substr(3, 6) << std::endl; // substring

	// Arrays
	int numbers[3];
	int length = sizeof(numbers)/sizeof(numbers[0]);
	for(int i = 0; i < length; i++)
	{
		numbers[i] = i * 9;
	}

	std::cout << numbers[2] << std::endl;

	return 0;
}

