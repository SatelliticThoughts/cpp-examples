#include <iostream>

int main(int argc, char **argv)
{
	// Basic if statement.
	if(true)
	{
		std::cout << "This will show" << std::endl;
	}

	// Basic if/else statement.
	if(false)
	{
		std::cout << "This will NOT show" << std::endl;
	}
	else
	{
		std::cout << "This will show" << std::endl;
	}

	// Basic if/else if/else statement.
	if(false)
	{
		std::cout << "This will NOT show" << std::endl;
	}
	else if(true)
	{
		std::cout << "This will show" << std::endl;
	}
	else
	{
		std::cout << "This will NOT show" << std::endl;
	}

	// &&, and
	if(true && true)
	{
		std::cout << "This will show" << std::endl;
	}

	if(true && false)
	{
		std::cout << "This will NOT show" << std::endl;
	}

	if(false && false)
	{
		std::cout << "This will NOT show" << std::endl;
	}

	// ||, or
	if(true || true)
	{
		std::cout << "This will show" << std::endl;
	}

	if(true || false)
	{
		std::cout << "This will show" << std::endl;
	}

	if(false || false)
	{
		std::cout << "This will NOT show" << std::endl;
	}

	int x = 8;
	int y = 2;

	// Equals
	if(x == y)
	{
		std::cout << "If x equals y this will show" << std::endl;
	}

	// Not Equal
	if(x != y)
	{
		std::cout << "If x does not equal y this will show" << 
			std::endl;
	}

	// Greater Than
	if(x > y)
	{
		std::cout << "If x is bigger than y this will show" << 
			std::endl;
	}

	// Less Than
	if(x < y)
	{
		std::cout << "If x is less than y this will show" << std::endl;
	}

	return 0;
}

