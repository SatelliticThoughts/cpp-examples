#include <iostream>

// function without parameters or return statement.
void sayHello()
{
	std::cout << "Hello" << std::endl;
}

// function with return statement.
std::string getWord()
{
	return "world";
}

// function with single parameter.
void doubleNumber(int x)
{
	std::cout << x * 2 << std::endl;
}

// function with multiple parameters and return statement.
int addNumbers(int x, int y)
{
	return x + y;
}

int main(int argc, char **argv)
{
	// Calling functions
	sayHello();
	std::cout << getWord() << std::endl;
	doubleNumber(6);
	std::cout << addNumbers(2, 5) << std::endl;

	return 0;
}

