#include <iostream>

int main(int argc, char **argv)
{
	bool running = true;
	int num = 0;

	// while loop
	while(running)
	{
		num += 1;
		if(num > 5)
		{
			running = false;
		}
	}

	// for loop
	for(int i = 1; i < 13; i++)
	{
		std::cout << "16 / " << i << " = " << 16 / i << std::endl;
	}

	int numbers[] = {1, 7, 2, 5, 8};
	
	// foreach loop
	for(int n : numbers)
	{
		std::cout << n << std::endl;
	}
	return 0;
}

