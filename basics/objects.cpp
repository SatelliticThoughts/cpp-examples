#include <iostream>

class Person
{
private:
	std::string name;
	int age;

public:
	// Getters
	std::string getName();
	int getAge();

	// Setters
	void setName(std::string name);
	void setAge(int age);

	Person(std::string name, int age); // Constructor
	~Person(); // Destructor
};

std::string Person::getName()
{
	return this->name;
}

int Person::getAge()
{
	return this->age;
}

void Person::setName(std::string name)
{
	this->name = name;
}

void Person::setAge(int age)
{
	this->age = age;
}

Person::Person(std::string name, int age)
{
	setName(name);
	setAge(age);
}

Person::~Person()
{
}

int main(int argc, char **argv)
{
	Person p = Person("luke", 1321);

	std::cout << "Name: " << p.getName() << " Age: " << p.getAge() << 
		std::endl;

	return 0;
}
